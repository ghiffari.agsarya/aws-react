#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })

for i in "${!array[@]}"; do    
    echo "Deploy project on server ${array[i]}"    
    ssh ubuntu@${array[i]} "cd react/ && git pull origin master"
done
